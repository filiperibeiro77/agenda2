import agenda.pessoa.Pessoa;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AgendaTeste {
    
    public AgendaTeste() {
    }
    
    private Pessoa umaPessoa;
    
    @Before
    public void setUp() throws Exception {
		umaPessoa = new Pessoa();
    }
    
    @Test
    public void testNome(){
        umaPessoa.setNome("Filipe");
        assertEquals("Filipe", umaPessoa.getNome());
    }
    
    @Test
    public void testTelefone(){
        umaPessoa.setTelefone("777-7777");
        assertEquals("777-7777", umaPessoa.getTelefone());
    }

    @Test
    public void testIdade(){
        umaPessoa.setIdade("20");
        assertEquals("20", umaPessoa.getIdade());
    }
    
    @Test
    public void testHangout(){
        umaPessoa.setHangout("firibeiro77");
        assertEquals("firibeiro77", umaPessoa.getHangout());
    }
    
    @Test
    public void testEndereco(){
        umaPessoa.setEndereco("Quadra 20 rua 5 lote 2 Gama");
        assertEquals("Quadra 20 rua 5 lote 2 Gama", umaPessoa.getEndereco());
    }

    @Test
    public void testSexo(){
        umaPessoa.setSexo("Masculino");
        assertEquals("Masculino", umaPessoa.getSexo());
    }
    
    @Test
    public void testEmail(){
        umaPessoa.setEmail("firibeiro77@live.com");
        assertEquals("firibeiro77@live.com", umaPessoa.getEmail());
    }
    
    @Test
    public void testRg(){
        umaPessoa.setSexo("888888");
        assertEquals("888888", umaPessoa.getRg());
    }
    
    @Test
    public void testCpf(){
        umaPessoa.setSexo("444444444");
        assertEquals("444444444", umaPessoa.getCpf());
    }
}
