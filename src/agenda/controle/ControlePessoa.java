package agenda.controle;
import agenda.pessoa.Pessoa;
import java.util.ArrayList;
public class ControlePessoa{
//Atributo
	private ArrayList<Pessoa> listaPessoas;
//Construtor
	public ControlePessoa(){
		listaPessoas = new ArrayList<Pessoa>();
	}
	
//Métodos
	public String adicionarPessoa(Pessoa umaPessoa){
		String mensagem = "Pessoa adicionada com sucesso!";
		listaPessoas.add(umaPessoa);
		return mensagem;
	}

	public String removerPessoa(Pessoa umaPessoa){
		String mensagem = "Pessoa removida com sucesso!";
		listaPessoas.remove(umaPessoa);
		return mensagem;
	}
	
	public Pessoa pesquisarNome(String umNome){
		for(Pessoa umaPessoa: listaPessoas){
			if(umaPessoa.getNome().equalsIgnoreCase(umNome)){
				return umaPessoa;
			}
		}
		return null;
	}	
	
	public void exibirPessoas(){
		for(Pessoa umaPessoa: listaPessoas){
			System.out.println(umaPessoa.getNome());
		}
	}
}